package Pong;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Tennis extends Applet implements Runnable, KeyListener {
    //applet is screen where we play the game in
    /*runnable makes it so we can run the applet and actually play the game
    runnable needs "public void run" implemented!
    */
    /* KeyListener is used to read key input for control
    needs 3 methods implemented: keyTyped, keyPressed, keyReleased. intellij tells you to implement these
    */

    final int WIDTH = 700; //define how wide the applet is, x coord
    final int HEIGHT = 500; //define how high the applet is, y coord
    HumanPaddle p1; //player 1
    AIPaddle p2; //player 2
    Thread thread; //global variable used in init()
    Ball b1; //ball class instantiated
    boolean gameStarted; //used to define if ball is already moving at start of the game or not

    public void init(){
        this.resize(WIDTH, HEIGHT); //enlarges the applet upto the specified ints
        this.addKeyListener(this); //adds the KeyListener, param can be null but won't throw exception
        gameStarted = false;
        p1 = new HumanPaddle(1); //humanPaddle needs a number for player
        p2 = new AIPaddle(2, b1);
        b1 = new Ball(); //create a ball
        thread = new Thread(this); //param needs a runnable object, this refers to Tennis
        thread.start(); //start method is needed to start the runnable
    }

    public void paint(Graphics g){
        //backdrop:
        g.setColor(Color.black); //set backdrop to black
        g.fillRect(0, 0, WIDTH, HEIGHT); // fill backdrop rectangle shaped from x=0, y=0 up to x=width, y=height

        if(b1.getX() < -10 || b1.getX() > 710) { //if ball has passed left border at x axis = -10 (center is at 10, see Ball class)
            // or if ball has passed right border at 710
            g.setColor(Color.red); //draw a String (below) in red color
            g.drawString("Game over", 350, 250); //drawString draws a String at desired x,y
        } else { //keeps drawing p1, p2 and ball if ball hasn't been past the borders
            //player 1:
            p1.draw(g); //draw() from HumanPaddle class

            //p2
            p2.draw(g); //draw() from AIPaddle class

            //ball:
            b1.draw(g);
        }

        //message showing before game is started
        if(!gameStarted){
            g.setColor(Color.red);
            g.drawString("Tennis", 340, 100);
            g.drawString("Press enter to begin", 310, 130);
        }

    }

    public void update(Graphics g){
        paint(g);
    }

    @Override
    public void run() {
        //needs to be implemented from the Runnable, keeps running the game infinitely
        for(;;) {
            if (gameStarted) {
                p1.move(); //has to move paddle of player 1 every loop
                p2.move();
                b1.move(); //runs the method to make the ball move
                b1.checkPaddleCollision(p1, p2);

                repaint();
                try {
                    Thread.sleep(10); //Thread: process that runs simultaniously with other Threads
                    // will pause game for 10 milliseconds
                    //sleep: param is milliseconds. sleep methods needs to be within a try catch! intellij will tell you
                } catch (InterruptedException e) { //automatic try catch for sleep method, doesn't catch runtime exc
                    e.printStackTrace(); //automatic try catch for sleep method
                }

            }
        }
    }

    public void keyTyped(KeyEvent e) { //auto generated when implementing KeyListener


    }

    public void keyPressed(KeyEvent e) { //auto generated when implementing KeyListener, what to do when pressing a key
        if(e.getKeyCode() == KeyEvent.VK_UP){ // if the keycode (int based on the key pressed, see doc) equals the keyevent for arrow key UP, VK stands for virtual keyboard
            p1.setUpAccel(true); //setUpAccel from HumanPaddle. accelerates up if pressing UP key

        } else if(e.getKeyCode() == KeyEvent.VK_DOWN) { //same as above, but for arrowkey DOWN
            p1.setDownAccel(true); //setDownAccel from HumanPaddle. accelerates down if pressing DOWN key
        } else if(e.getKeyCode() == KeyEvent.VK_ENTER){ //used to actually start playing once game is running
            gameStarted = true;
        }
    }

    public void keyReleased(KeyEvent e) { //auto generated when implementing KeyListener, what to do when a key is released (no longer pressed)
        if(e.getKeyCode() == KeyEvent.VK_UP){ //copied from keyPressed()
            p1.setUpAccel(false); //setUpAccel from HumanPaddle. stop accelerating UP when UP key is no longer pressed

        } else if(e.getKeyCode() == KeyEvent.VK_DOWN) {
            p1.setDownAccel(false); //setUpAccel from HumanPaddle. stop accelerating DOWN when DOWN key is no longer pressed
        }
    }
}
