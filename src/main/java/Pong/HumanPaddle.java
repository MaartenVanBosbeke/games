package Pong;

import java.awt.*;

public class HumanPaddle implements Paddle{

    double y; //coordinates
    double yVel; //velocity / speed
    boolean upAccel;  //moving upo faster?
    boolean downAccel; //moving down faster?
    int player; //player number 1 or 2
    int x;
    final double GRAVITY = 0.94;

    public HumanPaddle(int player){
        upAccel = false; //not moving at start
        downAccel = false; //not moving at start
        y = 210; //coordinate center for the paddle
        yVel = 0; //initial speed, not moving
        if(player == 1){
            x = 20; //player 1 will appear on coord x = 20, meaning left of screen
        } else {
            x = 660; //if player 2: appear on x = 660, right of screen
        }
    }


    public void draw(Graphics g) { //auto generated from implementing Paddle
        g.setColor(Color.white);
        g.fillRect(x, (int) y, 20, 80); //fill a rectangle with white color, starting at coord x,y, width is 20, height is 80
    }

    public void move() { //auto generated from implementing Paddle
        if(upAccel){ //if moving up
            yVel -= 2; //move up at speed of abs.value 2, but because it's moving up along y axis, it is going -2.
            //y0 is at top, y500 (height) is at bottom of axis
        } else if(downAccel){
            yVel += 2; //move up at speed of abs.value 2, but because it's moving down along y axis, it is going +2.
        } else if(!upAccel && !downAccel){
            yVel *= GRAVITY; //slow down at speed of GRAVITY, until it reaches 0. when yVel is 0, paddle stopped moving
        }

        if(yVel >=5){
            yVel = 5; //cap speed of moving paddle at 5, else it will keep accelerating
        } else if(yVel<=-5){
            yVel = -5; // same as above, in other direction
        }
        y += yVel; // moves the paddle to new y location when pressing key (input)

        if(y <0){ //if paddle has gone past top border (farther than 0), it should stop at 0
            y = 0;
        } else if (y >= 420){ //420: height is 500, paddle is 80 high, so paddle y0 should stop at 80 before 500 (500-80=420)
            y = 420;
        }


    }

    public void setUpAccel(boolean input){ //is accelerating/moving up? depends on input in KeyPressed
        upAccel = input;
    }

    public void setDownAccel(boolean input) { //is accelerating/moving down? depends on input in KeyPressed
        downAccel = input;
    }

    public int getY() { //auto generated from implementing Paddle
        return (int)y; //cast y to int
    }
}
