package Pong;

import java.awt.*;

//copied from humanPaddle with some adjustments
public class AIPaddle implements Paddle{

    double y; //coordinates
    double yVel; //velocity / speed
    boolean upAccel;  //moving upo faster?
    boolean downAccel; //moving down faster?
    int player; //player number 1 or 2
    int x;
    final double GRAVITY = 0.94;
    Ball b1; //needs to be instantiated so AI can use it

    public AIPaddle(int player, Ball b){ //added Ball b because ai moves depending on ball
        upAccel = false;
        downAccel = false;
        b1 = b;
        y = 210; //coordinate center for the paddle
        yVel = 0; //initial speed, not moving
        if(player == 1){
            x = 20; //player 1 will appear on coord x = 20, meaning left of screen
        } else {
            x = 660; //if player 2: appear on x = 660, right of screen
        }
    }


    public void draw(Graphics g) { //auto generated from implementing Paddle
        g.setColor(Color.white);
        g.fillRect(x, (int) y, 20, 80); //fill a rectangle with white color, starting at coord x,y, width is 20, height is 80
    }

    public void move() { //auto generated from implementing Paddle
        y = b1.getY() - 40; //y coord of AI paddle equals y coord of ball, but -40 so it uses the center of the paddle (instead of upper leftmost corner)

        if(y <0){ //if paddle has gone past top border (farther than 0), it should stop at 0
            y = 0;
        } else if (y >= 420){ //420: height is 500, paddle is 80 high, so paddle y0 should stop at 80 before 500 (500-80=420)
            y = 420;
        }


    }

    public void setUpAccel(boolean input){ //is accelerating/moving up? depends on input in KeyPressed
        upAccel = input;
    }

    public void setDownAccel(boolean input) { //is accelerating/moving down? depends on input in KeyPressed
        downAccel = input;
    }

    public int getY() { //auto generated from implementing Paddle
        return (int)y; //cast y to int
    }
}
