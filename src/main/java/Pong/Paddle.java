package Pong;

import java.awt.*;

public interface Paddle { //interface will be implemented in HumanPaddle class

    public void draw(Graphics g);
    public void move();
    public int getY();
}
