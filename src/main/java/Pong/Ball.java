package Pong;

import java.awt.*;

public class Ball {
    private double xVel; //both x and y because ball moves in up/down AND left/right, paddles only moved up/down
    private double yVel;
    private double x; //samen reasoning as with the velocity
    private double y;



    public Ball(){
        x = 350; //starting in center of x axis (x in applet goes to 700)
        y = 250; //starting in center of y axis (y goes to 500)
        xVel = getRandomSpeed() * getRandomDirection();// video starts by saying "-2" => move along x axis by -2, so going left
        //is changed later to this, so the xVel is a random speed (3-5) and a random direction (left-right)
        yVel = getRandomSpeed() * getRandomDirection(); //same as xVel moving along y axis by 1, so going down
    }

    public double getRandomSpeed(){
        return (Math.random() *3 +2);
        /*
        random returns a number between 0 and 1
        *3 to return between 0 and 3
        +2 to return between 2 and 5
         */
    }

    public int getRandomDirection(){
        int rand = (int)(Math.random() *2); // gives random int number between 0 and 2
        if(rand == 1){
            return 1; //makes ball go right
        }else{
            return -1; //makes ball go left
        }
    }

    public void checkPaddleCollision(Paddle p1, Paddle p2){
        //p1:
        if(x <=50){ //if position of ball is same as the right border of the left paddle
            //right border of left paddle is 50, because:
            // 20 (space between border of applet and left border of paddle
            // + 20 (width of paddle)
            // + 10 (to center of ball)
            if(y >= p1.getY() && y <= p1.getY() + 80){ //checks if ball is on the paddle or not
                //if y (position ball along y axis) is below the top of the paddle (p1.getY)
                // AND is above the bottom of the paddle (p1.getY + 80 because paddle is 80 high, so bottom will be the top + 80)
                xVel = -xVel; // change the direction along x axis when the ball hits the paddle
            }
        }

        //p2:
        if(x >= 650) { //if position of ball is same as left border of the right paddle, opposite of above (for left paddle)
            if (y >= p2.getY() && y <= p2.getY() + 80) {
                xVel = -xVel;
            }
        }
    }

    public void draw(Graphics g){ //draws a ball
        g.setColor(Color.white); //set color of the ball
        g.fillOval((int)x-10, (int)y-10, 20, 20);
        /*
        (int)x-10: x and y are doubles, so need to be cast to ints (fillOval works with int, not double values)
        -10 because x and y start at left upper corner, but we need to use the "center" of the ball,
        when width and height are 0-20, center is at 10
         */
    }

    public void move(){ //changes positions on x, y, by values in xVel and yBel
        x += xVel; //change position on x axis by the xVel, so depending on position given in public Ball():
        // on position x=350, next position will be x=348, x=346, etc
        y += yVel; //change position on y axis by the yVel, so depending on position given in public Ball():
        // on position y=250, next position will be y=251, y=252, etc

        if(y < 10){ // center of ball has been defined at "10" in draw() method, so we need to check where the center of our ball is ending up
            //if center of ball (10) is at a position y < 10, it has passed the upper border
            yVel = -yVel; // change the velocity of yVel to its opposite (+ changes to -, - changes to +), so change the direction along y axis
        } else if (y > 490){ // if center of ball (10) passes 490, it has reached bottom border at 500, so direction needs to be changed
            yVel = -yVel;
        }
    }

    public int getX(){
        return (int) x;
    }

    public int getY(){
        return (int) y;
    }

}
