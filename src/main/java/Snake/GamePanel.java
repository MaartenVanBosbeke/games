package Snake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;

public class GamePanel extends JPanel implements ActionListener {

    static final int SCREEN_WIDTH = 600; //600 pixels wide
    static final int SCREEN_HEIGHT = 600; //600 pixels high
    /* example=
            x-axis ->
            -------------------------------------------
    y-axis  0x, 0y      |   300x, 0y     |   600x, 0y
    |       -------------------------------------------
    V       0x, 300 y   |   300x, 300y   |   600x, 300y
            -------------------------------------------
            0x, 600y    |   300x, 600y   |   600x, 600y
            -------------------------------------------
    */
    static final int UNIT_SIZE = 25; //25 pixels by 25 pixels per "box" unit, so total of 24 units per screen_width or height
    static final int GAME_UNITS = (SCREEN_WIDTH*SCREEN_HEIGHT)/UNIT_SIZE; //24*24 amount of units on screen, grid of 24x24
    static int DELAY = 100; //how fast the game is running: 0 is fastest, higher number makes it go slower

    final int x[] = new int[GAME_UNITS]; //"length" of 24 units
    final int y[] = new int[GAME_UNITS]; //"height" of 24 units

    int bodyParts = 6; //6 bodyparts on snake to start with
    int applesEaten; //initial is 0
    int appleX; //x coordinate of where apple is, going to appear randomly
    int appleY; //y coordinate of where apple is, going to appear randomly
    char direction = 'R'; //begin by going right
    boolean running = false;
    Timer timer; //instance of timer class
    Random random; // instance of random class

    boolean grid = false; //change to true to enable a drawn grid, see draw()

    GamePanel(){
        random = new Random();//new instance of Random class
        this.setPreferredSize(new Dimension(SCREEN_WIDTH, SCREEN_HEIGHT));
        // setPrefferedSize method already exists
        // Dimension is new instance of class Dimension, needs two params: width and height
        this.setBackground(Color.black); //setBackground method already exists, with param Color, black is a choice but already created
        this.setFocusable(true); //method already exists
        this.addKeyListener(new MyKeyAdapter()); //addKey already exists, MyKeyAdap is created below
        startGame();
    }

    public void startGame(){
        newApple(); //call this method to set a new apple on screen
        running = true; //change the boolean (standard false) to true to start the game
        timer = new Timer(DELAY, this); //new instance of Timer, 1st param is an int, in this case our DELAY
        //2nd param is a "listener", using "this"
        timer.start(); //start method is from the Timer class
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        draw(g);
    }

    public void draw(Graphics g) {
        if (running) { //if the game is runnin (started at false)

            //TURN SCREEN INTO GRID by drawing lines (defined by width, height, unit size)
            // disabled by default, turn boolean grid into true to enable grid
            if(grid) {
                for (int i = 0; i < SCREEN_HEIGHT / UNIT_SIZE; i++) {
                    g.drawLine(i * UNIT_SIZE, 0, i * UNIT_SIZE, SCREEN_HEIGHT); //DRAW VERTICAL LINES, only on x coords
                    // method from Graphics class, draw a line in coor x1, y1, x2 and y2
                    g.drawLine(0, i * UNIT_SIZE, SCREEN_WIDTH, i * UNIT_SIZE); //DRAW HORIZONTAL LINES, only along y coords
                }
            }


            g.setColor(Color.red); //set apple to RED
            g.fillOval(appleX, appleY, UNIT_SIZE, UNIT_SIZE); //fill oval shape of coord x1, y1, x2, y2: x, y, unit, unit

            for (int i = 0; i < bodyParts; i++) {
                if (i == 0) { // for the HEAD of the snake
                    g.setColor(Color.green);
                    g.fillRect(x[i], y[i], UNIT_SIZE, UNIT_SIZE); //fill rectangle, coord x, y, width, height.
                    // width and height is unit size, because the head is only one unit big

                } else { //set rest of snake color
                    g.setColor(new Color(45, 180, 0)); //create a new color with the 3 RGB values

                    //change snake color into random int value, RGB between 0 and 255
                    /*
                    g.setColor(new Color(random.nextInt(255), random.nextInt(255), random.nextInt(255)));
                    */

                    g.fillRect(x[i], y[i], UNIT_SIZE, UNIT_SIZE); //fill the whole snake with the color above
                }
            }
            //explaination on this: see gameOver();
            //show score
            g.setColor(Color.white);
            g.setFont(new Font("ink free", Font.BOLD, 15));
            FontMetrics metrics = getFontMetrics(g.getFont());
            g.drawString("Score: " + applesEaten, (0 + metrics.stringWidth("Score: " + applesEaten))/2, g.getFont().getSize());
        } else { //if game is not running anymore (see checkCollisions for cases)
            gameOver(g);
        }
    }

    public void newApple(){ //generate coord of new apple
        appleX = random.nextInt((int)(SCREEN_WIDTH/UNIT_SIZE))*UNIT_SIZE; //get a random number for the x coord
        /*random.nextInt is automatic method, returns int between 0 and specified value
        cast into an (int) in case of double value when dividing width/size
        WIDTH/UNIT so apple can only generate between 0 and max x coords (not outside grid)
        * unit size so it is nicely within a single square on the grid.
        else if width is 100, unit size 5, it would generate between 0 and 20, not between 0 and 100
        */
        appleY = random.nextInt((int)(SCREEN_HEIGHT/UNIT_SIZE))*UNIT_SIZE; // get a random number for y coord
    }

    public void move(){
        for (int i = bodyParts; i > 0 ; i--) {
            x[i] = x[i-1]; //shifting all coord by -1 ?
            y[i] = y[i-1];
        }
        switch(direction){ //change direction of snake's head
            case 'U':
                y[0] = y[0] - UNIT_SIZE; //move along vertical y: if at position y=3, move to y=2 by the space of the unit size
                break;
            case 'D':
                y[0] = y[0] + UNIT_SIZE; //move along vert y: if at position y=3 move to y=4
                break;
            case 'R':
                x[0] = x[0] + UNIT_SIZE; //move along horizontal x: if at x=3, move to x=4
                break;
            case 'L':
                x[0] = x[0] - UNIT_SIZE; //move along horizontal x: if at position x=3, move to x=2 by the space of the unit size
                break;
        }

    }

    public void checkApple(){
        if((x[0] == appleX) && (y[0] == appleY)){ //if head of snake (x[0], y[0]) is at same location as apple(appleX, appleY)
            bodyParts++; //grow body by 1 part, or use +=1; +=3; +=10; ...
            applesEaten++; // counts as a "score"
            DELAY--; //increase speed
            newApple(); //generate a new apple
        }

    }

    public void checkCollisions(){ //check if head of snake collides with ...
        //head collides with body?
        for (int i = bodyParts; i > 0; i--) {
            if((x[0] ==x[i]) && (y[0] == y[i])) { //if head position (x,y) of the snake == current index: it has collided with its own body
                //example: piece of body is at 3,4 (x,y); and head is also at 3,4, the head is ON another piece its body => game over
                running = false;
            }
        }

        //head touches left border?
        if(x[0] < 0){ //if head is at position -1, it has "left the grid", in other words, passed/collided the border
            running = false;
        }

        // head touches right border?
        if(x[0] > SCREEN_WIDTH){ //went past the border on the right, width defines where the right border is
            running = false;
        }
        // head touches top border?
        if(y[0] < 0) {
            running = false;
        }

        //head touches bottom border?
        if(y[0] > SCREEN_HEIGHT){
            running = false;
        }

    }

    public void gameOver(Graphics g){
        //Game over text
        g.setColor(Color.red);
        g.setFont(new Font("ink free", Font.BOLD, 75));
        FontMetrics metrics = getFontMetrics(g.getFont());
        g.drawString("Game Over", (SCREEN_WIDTH - metrics.stringWidth("Game Over"))/2, (SCREEN_HEIGHT/2));
        /*drawString:
            draw the String in first param: "Game Over"
        on location x and y:
            x (2nd param) is the width of the screen - the width of the string to draw, divided by 2
            example= width is 600, string is 100. (600-100)/2 draws the string at x=250 until x=350, in the middle of our 600 width
            y (3rd param) : middle of the string is coord y, so that should be in the middle of our height, hence screen_height/2
        */

        // game over score
        g.setColor(Color.red.brighter());
        g.setFont(new Font("ink free", Font.BOLD, 40));
        FontMetrics metrics2 = getFontMetrics(g.getFont()); //metrics2 because already used "metrics" for gameover text
        g.drawString("Score: " + applesEaten, (SCREEN_WIDTH - metrics2.stringWidth("Score: " + applesEaten))/2, g.getFont().getSize());

    }

    public class MyKeyAdapter extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e){

            switch(e.getKeyCode()){ //switch is going to examine the KeyEvent

                case KeyEvent.VK_LEFT: //in case left arrow key is pressed (KeyEvent.VK_LEFT is implicit method, don't have to create this)
                    if(direction != 'R') { //if snake is not already going right (can't do a 180° turn)
                        //no need to check if('U' or 'D'), snake can turn left when already U or D
                        direction = 'L'; //turn in left direction, see move();
                    }
                    break;
                case KeyEvent.VK_RIGHT: // in case right arrow key is pressed
                    if(direction != 'L') { //if snake is not already going left (can't do a 180° turn)
                        direction = 'R'; //turn in right direction, see move();
                    }
                    break;
                case KeyEvent.VK_UP:
                    if(direction != 'D') {
                        direction = 'U';
                    }
                    break;
                case KeyEvent.VK_DOWN:
                    if(direction != 'U') {
                        direction = 'D';
                    }
                    break;
            }

        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(running){ //only when the game is running
            move(); //switch with the movement directions
            checkApple();
            checkCollisions();
        }
        repaint(); //change color when the game is no longer running
    }
}
